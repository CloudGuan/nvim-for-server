# nvim-for-server

## 介绍
ycm 安装环境老是要手动调整，每次换新机器或者自己折腾系统完了之后需要自己修改配置一次linux的环境很麻烦，neovim的社区一直比较活跃，就尝试转了一下，发现是真的很香，为了方便自己之后使用，就做了这个半自动化配置文件

## 软件依赖
**下载不下来的时候点击这里下载放到安装目录就不用会在下载了会直接解压安装！！！！！！！！**
> - [neovim 4.x+](https://github.com/neovim/neovim/releases/latest/download/nvim.appimage)
> - [nodejs](https://install-node.now.sh/latest)
> - [clangd](https://github.com/clangd/clangd/releases/download/11.0.0/clangd-linux-11.0.0.zip)
> - [glibc 2.18+](http://ftp.gnu.org/gnu/glibc/glibc-2.18.tar.gz)
> - [python3.x](https://www.python.org/ftp/python/3.7.6/Python-3.7.6.tgz)
> - libfuse 


## 安装教程
1. 切换到root 用户执行如下命令 中间安装nodejs的时候需要你输入一次 y
```shell 
curl https://gitee.com/CloudGuan/nvim-for-server/raw/master/init_my_vim.sh | bash
```
2. 确认安装成功后切换回你的非root用户安装
```shell
curl https://gitee.com/CloudGuan/nvim-for-server/raw/master/init_my_config.sh | bash
```
3. 全部安装成功之后启动vim，这个时候第一次启动我的配置会自动检测有没有相关插件，进行安装
4. 如果安装失败 vim 命令模式执行 :PlugInstall
5. 命令模式执行 :UpdateRemotePlugins 用于更新dfex目录树
6. 命令模式执行 :CocInstall coc-clangd coc-sh 
enjoy

## 使用说明
TODO 添加快捷键说明 操作说明

## 安装脚本详细说明 **这一切上面的脚本都给你做了！！！ 万一遇到问题可以使用这个进行逐步安装查错！！！**
> 如果安装出现中断你可以自行下载相关的依赖文件进行，然后再执行脚本，我会跳过下载步骤进行安装，国内的云可能下载不下来，我这里在上门已经把相关连接贴了出来

### Python3.x

这个自己已经有安装脚本了，如果是自己编译安装，要注意centos7上需要拷贝 动态库，以及yum urlgrabber-ext-down 文件的修改，安装的话可以调用这个脚本

```shell
curl https://gitee.com/CloudGuan/bashboot/raw/master/python_boot.sh | bash 
```

这边比较难受的是国内的下载速度一直比较慢，我脚本里面curl和wget都尝试了 , 还是下载不下来，所以建议自行下载

安装完毕之后升级一下你的pip && pip3 

```shell
pip3 install --upgrade pip
```

###  libfuse 
```shell
 yum install fuse fuse-devel
```
neovim 的依赖

### defx目录树环境 

```shell
pip3 install pynvim
pip3 install pygments
```
### nodejs 支持

```shell
curl --fail -LSs https://install-node.now.sh/latest | sh
```

## neovim 安装

neovim的安装确实特别简单，appimage 已经免除了需要自己编译的各种问题，在下载后确保你能正确启动之后，可以yum remove vim 删除掉你原本的vim，开始使用neovim了

```shell
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage

yum remove vim
mv nvim.appimage /usr/bin/nvim.appimage 
if [ -f /bin/vim ];
then
	rm /bin/vim
fi 
ln -s /usr/bin/nvim.appimage /bin/vim
```

### 支持language server 
> example clangd
```shell
yum install zip
wget https://github.com/clangd/clangd/releases/download/11.0.0/clangd-linux-11.0.0.zip
unzip clangd-linux-11.0.0.zip
mv clangd_11.0.0/ /usr/local/clang
ln -s /usr/local/clang/bin/clangd /usr/bin/clangd
```

注意这里下载安装clangd 需要 glibc 2.18 要嘛你自己 编译clangd 我建议你选glibc升级 

这里我**送**你glibc的安装脚本

```shell
wget http://ftp.gnu.org/gnu/glibc/glibc-2.18.tar.gz
tar -xvf glibc-2.18.tar.gz 
cd glibc-2.18
mkdir build && cd build && ../configure --prefix=/usr && make -j4 && make install
```

### 启动nvim

第一次启动的时候需要在命令行界面输入：PluginInstall 安装所有的插件，退出在进入一次就可以看到已经设置好的nvim配置了

使用:UpdateRemotePlugins 来更新一下远端的代码配置

接下来我将详细叙述c++ lsp server 的安装过程如果你有需要安装别的插件，可以自行在这个网页扩展

- 安装c++代码补全

  ```shell
  vim -c 'CocInstall -sync coc-clangd'
  ```
- 生成`compile_commands.json` 文件  

  cmake 脚本中第一次使用lsp需要自行添加一行 用于生成compile_commands .json 文件 
  ```shell
  cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1
  ```

  然后拷贝这个文件到你的工程的根目录就好了注意这个只需要生成一次

## FAQ
### glibc.so 修改dll的连接路径顺序
如果你没有升级过glibc 但是 又自己编译安装了libstdc++，并且自己重新定义过so的路径查找顺序，安装后可能回出现找到旧版本so的问题，把你之前的操作重新做一次就可以了，或者glibc.so 所在目录的 libstdc++ 采用软连接的形式指向你自己的libstdc++.so 